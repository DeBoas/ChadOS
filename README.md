# ChadOS
## Description 📖
A bloat-free OS that does all the work without fail
## Run ⚙
### Compile own binary
- *1 - Clone this repo*
- *2 - Build the bin*

    `nasm -f bin src/os.asm -o os.bin`
- *3 - Run with qemu*

    `qemu-system-x86_64 os.bin`
### Run online
![Here](https://replit.com/@DeBoas/ChadOS?v=1)
## Features 📨
- `✅ Boot`
- `✅ Shutdown`
## To Do 📑
- `[ ] None, because this is perfect`